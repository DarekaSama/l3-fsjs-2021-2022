var express = require('express');
var router  = express.Router();
const jokeController = require('../controllers/randomJokeController');
router.get('/', jokeController.random);
router.get('/:id', jokeController.random2);
module.exports.router = router;