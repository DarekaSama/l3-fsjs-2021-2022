const jokeJS = require("../data/jokes.json");
module.exports.random = (requete, reponse) => {
    const id_value = Math.floor(Math.random() * (jokeJS.length - 0 ) + 0);
    reponse.render('pages/random', {joke : jokeJS[id_value].joke , path : "/random/" + id_value});
};
module.exports.random2 = (requete, reponse) => {
    let id_value = requete.params.id;
    reponse.render('pages/random', {joke : jokeJS[id_value].joke , path : "/random/" + id_value});
};