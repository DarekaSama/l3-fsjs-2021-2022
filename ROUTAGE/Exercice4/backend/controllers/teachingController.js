module.exports.home = (requete, reponse) => {
    reponse.render('pages/teaching');
};
module.exports.javascript = (requete, reponse) => {
    reponse.render('pages/teaching/javascript');
};
module.exports.php = (requete, reponse) => {
    reponse.render('pages/teaching/php');
};
module.exports.node = (requete, reponse) => {
    reponse.render('pages/teaching/node');
};
module.exports.express = (requete, reponse) => {
    reponse.render('pages/teaching/node/express');
};