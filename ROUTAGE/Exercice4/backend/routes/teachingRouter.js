var express = require('express');
var router  = express.Router();
const teachingController = require('../controllers/teachingController')
router.get('/',teachingController.home);
router.get('/javascript',teachingController.javascript);
router.get('/php',teachingController.php);
router.get('/node',teachingController.node);
router.get('/node/express',teachingController.express);
module.exports.router = router;