var express = require('express');
var router  = express.Router();
const aboutController = require('../controllers/aboutController');
router.get('/',aboutController.home);
module.exports.router = router;