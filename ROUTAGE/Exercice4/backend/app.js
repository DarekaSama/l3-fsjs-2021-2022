const express = require('express');
const app = express();        // Instanciation d'une application express
const path = require("path"); // inclusion d'express
const expressLayouts = require('express-ejs-layouts'); // inclusion express Layout
app.use(express.static(path.join(__dirname, 'public')));
// Definition moteur de rendu
app.set('view engine', 'ejs');
// Declaration dossier des vues
app.set('views', path.join(__dirname, 'views'));
//middleware
app.use(expressLayouts);
//layout par defaut
app.set('layout', '../views/layouts/layout');

//routage
var route1 = require('./routes/aboutRouter');
var route2 = require('./routes/homeRouter');
var route3 = require('./routes/teachingRouter');

app.use('/about', route1.router);
app.use('/home', route2.router);
app.use('/teaching', route3.router);


// Middleware TRIGGER par requete
app.use((requete,reponse, next) => {
    const now = new Date().toDateString();
    console.log(`${now} := Une requete ${requete.method} a est arrivee.`);
    next();
});

// Route TRIGGER GET
app.get("/", (requete, reponse) =>{
    reponse.render('pages/home');
});
/*
app.get("/research", (req, reponse) => {
    reponse.render('pages/research');
    //res.send("<h1>Travaux de Recherche</h1>");
});

*/

// Route TRIGGER GET => autres
app.get("*", (requete, reponse) =>{
    reponse.redirect('/');
});
// Exportation de notre application express
module.exports = app;