const express = require('express');
const app = express();        // Instanciation d'une application express
const path = require("path"); // inclusion d'express
const expressLayouts = require('express-ejs-layouts'); // inclusion express Layout
app.use(express.static(path.join(__dirname, 'public')));
// Definition moteur de rendu
app.set('view engine', 'ejs');
// Declaration dossier des vues
app.set('views', path.join(__dirname, 'views'));
//middleware
app.use(expressLayouts);
//layout par defaut
app.set('layout', '../views/layouts/layout');

app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Middleware TRIGGER par requete
app.use((requete,reponse, next) => {
    const now = new Date().toDateString();
    console.log(`${now} := Une requete ${requete.method} a est arrivee.`);
    next();
});

// Route TRIGGER GET
app.get("/", (requete, reponse) =>{
    let query = requete.query;
    reponse.render('pages/form', { query });
});

app.get("/form", (requete, reponse, next) => {
    reponse.render('pages/smpl_form');
});
app.post("/form", (requete, reponse, next) => {
    let data = requete.body;
    reponse.render('pages/show_post_data.ejs', { data });
});

// Route TRIGGER GET => autres
app.get("*", (requete, reponse) =>{
    reponse.redirect('/form');
});
// Exportation de notre application express
module.exports = app;