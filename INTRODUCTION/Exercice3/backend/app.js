const express = require('express');
const app = express();        // Instanciation d'une application express
const path = require("path"); // inclusion d'express
const expressLayouts = require('express-ejs-layouts'); // inclusion express Layout

app.use(express.static(path.join(__dirname, 'public')));
// Definition moteur de rendu
app.set('view engine', 'ejs');
// Declaration dossier des vues
app.set('views', path.join(__dirname, 'views'));
//middleware
app.use(expressLayouts);
//layout par defaut
app.set('layout', '../views/layouts/layout');
// Traitement
app.use((requete, reponse) => {
    // Demande Rendu EJS
    reponse.render('pages/home', { nickname: 'Louis', sexe:'femme'});
});

// Exportation de notre application express
module.exports = app;