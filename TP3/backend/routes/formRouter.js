var express = require('express');
var router  = express.Router();
const formController = require('../controllers/formController');
router.get('', formController.login);
router.post('', formController.logged);
module.exports.router = router;