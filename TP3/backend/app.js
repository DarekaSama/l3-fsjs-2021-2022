const express = require('express');
const app = express();        // Instanciation d'une application express
const path = require("path"); // inclusion d'express
const fs = require("fs");
const expressLayouts = require('express-ejs-layouts'); // inclusion express Layout
app.use(express.static(path.join(__dirname, 'public')));
// Definition moteur de rendu
app.set('view engine', 'ejs');
// Declaration dossier des vues
app.set('views', path.join(__dirname, 'views'));
//middleware
app.use(expressLayouts);
//layout par defaut
app.set('layout', '../views/layouts/layout');

app.use(express.json());
app.use(express.urlencoded({extended: false}));

//routage
const home = require('./routes/homeRouter');
const form = require('./routes/formRouter');

app.use('/', home.router);
app.use('/form', form.router);

// Route TRIGGER GET => autres
app.get("*", (requete, reponse) =>{
    reponse.redirect('/');
});
// Exportation de notre application express
module.exports = app;